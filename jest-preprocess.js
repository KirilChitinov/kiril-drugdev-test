const babelOptions = {
  presets: [
    '@babel/preset-react',
    "@babel/preset-env",
    "@babel/preset-typescript",
  ],
  plugins: ["@babel/plugin-transform-runtime"]
};

module.exports = require("babel-jest").createTransformer(babelOptions)
