export const isEmailValid = (email: string): boolean => {
  return /^.+@.+\..+$/.test(email)
}

export const isNameValid = (name: string): boolean => {
  return name.length > 0
}
