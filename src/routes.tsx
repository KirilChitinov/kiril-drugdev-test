import React from 'react'
import { Route } from 'react-router-dom'
import AppContainer from './components/AppContainer'
import ContactsList from './components/ContactsList'
import UpdateContact from './components/UpdateContact'
import ViewContact from './components/ViewContact'

const Routes = (
  <AppContainer>
    <Route component={ContactsList} path='/' exact/>
    <Route component={UpdateContact} path='/contact/update/:id' />
    <Route component={ViewContact} path='/contact/view/:id' />
  </AppContainer>
)

export default Routes