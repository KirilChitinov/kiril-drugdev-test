import React, { FC, useState } from 'react'
import { useMutation } from '@apollo/react-hooks'
import { Alert } from '@material-ui/lab'
import { ADD_CONTACT } from '../../graphQL/mutations'
import { GET_CONTACTS } from '../../graphQL/queries'
import { isEmailValid, isNameValid } from '../../helpers/validation'
import Contacts from '../../interfaces/contacts'
import ContactForm from '../ContactForm'

const AddContact: FC = () => {
  const [email, setEmail] = useState('')
  const [emailError, setEmailError] = useState(false)

  const [name, setName] = useState('')
  const [nameError, setNameError] = useState(false)

  const [addContact] = useMutation(ADD_CONTACT,
    {
      update(cache, { data: { addContact } }) {
        const {contacts} = cache.readQuery<Contacts>({ query: GET_CONTACTS })!
        cache.writeQuery({
          query: GET_CONTACTS,
          data: { contacts: contacts.concat([addContact]) }
        })
      }
    })

  const _handleAddContact = () => {
    if (!isNameValid(name)) {
      setNameError(true)
      return false
    }

    if (!isEmailValid(email)) {
      setEmailError(true)
      return false
    }

    const contact = { name, email };
    addContact({ variables: { contact } })
    setEmail('')
    setName('')
  }

  const _renderEmailError = () => {
    return (
      <Alert severity='warning'>
        Invalid email — check it out!
      </Alert>
    )
  }

  const _renderNameError = () => {
    return (
      <Alert severity='warning'>
        Invalid name — field can not be empty!
      </Alert>
    )
  }

  const _handleEmailInput = (e: any) => {
    emailError && setEmailError(false)
    setEmail(e.target.value)
  }

  const _handleNameInput = (e: any) => {
    nameError && setNameError(false)
    setName(e.target.value)
  }

  return (
    <>
      {nameError && _renderNameError()}
      {emailError && _renderEmailError()}
      <ContactForm
        name={name}
        email={email}
        buttonText='Add contact'
        submitForm={_handleAddContact}
        changeName={_handleNameInput}
        changeEmail={_handleEmailInput}
      />
    </>
  )
}

export default AddContact