import React, { FC } from 'react'
import { Button, Grid, TextField } from '@material-ui/core'
import './styles.css'

export interface Props {
  name: string
  email: string
  buttonText: string
  submitForm: any
  changeName: any
  changeEmail: any
}

const ContactForm: FC<Props> = ({name, email, buttonText, submitForm, changeName, changeEmail}) => (
  <form
    className='contact-form'
    data-testid='contact-form'
    onSubmit={e => {
      e.preventDefault()
      submitForm()
    }}
  >
    <Grid container spacing={2}>
      <Grid item xs={6} sm={4} className='contact-details-container'>
        <TextField
          inputProps={{ 'data-testid': 'name-input' }}
          label='Name'
          name='name'
          value={name}
          variant='outlined'
          className='contact-details-input'
          onChange={changeName}
        />
      </Grid>

      <Grid item xs={6} sm={4} className='contact-details-container'>
        <TextField
          inputProps={{ 'data-testid': 'email-input' }}
          label='Email'
          name='email'
          value={email}
          variant='outlined'
          className='contact-details-input'
          onChange={changeEmail}
        />
      </Grid>

      <Grid item xs={12} sm={4} className='submit-button-container'>
        <Button
          type='submit'
          data-testid='submit'
          color='default'
          variant='outlined'
          className='submit-button'
        >
          {buttonText}
        </Button>
      </Grid>
    </Grid>
  </form>
)

export default ContactForm

