import React, { FC } from 'react'
import { Grid } from '@material-ui/core'
import AppHeader from '../AppHeader'
import './styles.css'

const AppContainer: FC = (props) => {
  return (
    <>
      <AppHeader title='Contacts manager'/>
      <Grid className='app-container' container>
        <Grid className='app' item xs={10}>
          {props.children}
        </Grid>
      </Grid>
    </>
  )
}

export default AppContainer
