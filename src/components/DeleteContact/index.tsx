import React, { FC, ReactNode } from 'react'
import { useMutation } from '@apollo/react-hooks'
import { useHistory } from 'react-router-dom'
import { DELETE_CONTACT } from '../../graphQL/mutations'
import { GET_CONTACTS } from '../../graphQL/queries'
import Contacts from '../../interfaces/contacts'

const DeleteContact: FC<{contactId: string, buttonText?: string, children?: ReactNode}> = ({ contactId, buttonText, children }) => {
  let history = useHistory()
  const [deleteContact] = useMutation(DELETE_CONTACT,
    {
      update(cache, { data: { deleteContact } }) {
        let {contacts} = cache.readQuery<Contacts>({ query: GET_CONTACTS })!
        const index = contacts.findIndex(c => c.id === contactId)
        contacts = [...contacts.slice(0, index), ...contacts.slice(index + 1)]
        cache.writeQuery({
          query: GET_CONTACTS,
          data: { contacts }
        })
      },
      onCompleted: () => { _redirectToHomeScreen() }
    })

  const _handleDeleteContact = (e: any) => {
    const id = contactId
    e.preventDefault()
    window.confirm('About to delete this contact. Are you sure?') && deleteContact({ variables: { id } })
  }

  const _redirectToHomeScreen = () => {
    history.push('/')
  }

  return (
    <span onClick={_handleDeleteContact}>
      {children}
      {buttonText}
    </span>
  )
}

export default DeleteContact
