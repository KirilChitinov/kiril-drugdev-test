import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { IconButton } from '@material-ui/core'
import PeopleIcon from '@material-ui/icons/People'
import './styles.css'

const AppHeader: FC<{title: string}> = ({title}: {title: string}) => {
  return (
    <header className='app-header'>
      <Link to='/' className='app-logo'>
        <IconButton className='app-icon-container'>
          <PeopleIcon className='app-icon' />
        </IconButton>
      </Link>
      <h1 className='app-title'>{title}</h1>
    </header>
  )
}

export default AppHeader
