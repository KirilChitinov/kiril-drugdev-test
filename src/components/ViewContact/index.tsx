import React, { FC } from 'react'
import { Alert } from '@material-ui/lab'
import { Button, ButtonGroup, Card, Container } from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks'
import { useParams } from 'react-router-dom'
import { GET_CONTACT } from '../../graphQL/queries'
import Contact from '../../interfaces/contact'
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import DeleteContact from '../DeleteContact'
import './styles.css'

const ViewContact: FC = (props: any) => {
  const { id } = useParams()
  const { loading, error, data } = useQuery<{contact: Contact}>(GET_CONTACT, { variables:  { id } })

  const _redirectToUpdateScreen = () => {
    props.history.push(`/contact/update/${id}`)
  }

  if (loading) return <p>Loading...</p>
  if (error) return <p>Error fetching your contact. Are you running your GQL server?</p>

  return (
    <>
      {data && data.contact ? (
        <Container className='contact-card-container'>
          <h2 className='contact-card-title'>Contact details</h2>
          <Card className='contact-card'>
            <div className='icon-container'>
              <EmojiPeopleIcon className='icon'/>
            </div>
            <p className='contact-card-details'>Contact ID: {data.contact.id}</p>
            <p className='contact-card-details'>Email: {data.contact.email}</p>
            <p className='contact-card-details'>Name: {data.contact.name}</p>
          </Card>
          <ButtonGroup color='primary' aria-label='text primary button group'>
            <Button color='primary' variant='outlined' onClick={_redirectToUpdateScreen}>Update contact</Button>
            <Button color='secondary' variant='outlined'>
              <DeleteContact contactId={data.contact.id} buttonText='Delete contact'/>
            </Button>
          </ButtonGroup>
        </Container>
      ) : (
        <Alert severity='error'>Contact not found!</Alert>
      )}
    </>
  )
}

export default ViewContact
