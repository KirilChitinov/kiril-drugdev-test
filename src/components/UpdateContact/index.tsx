import React, { FC, useState } from 'react'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { useParams } from 'react-router-dom'
import { GET_CONTACT } from '../../graphQL/queries'
import { UPDATE_CONTACT } from '../../graphQL/mutations'
import { Container } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { isEmailValid, isNameValid } from '../../helpers/validation'
import Contact from '../../interfaces/contact'
import ContactForm from '../ContactForm'
import './styles.css'

const UpdateContact: FC = (props: any) => {
  const [email, setEmail] = useState('')
  const [emailError, setEmailError] = useState(false)

  const [name, setName] = useState('')
  const [nameError, setNameError] = useState(false)

  const { id } = useParams()
  const { loading, error, data } = useQuery<{contact: Contact}>(GET_CONTACT, { variables:  { id }, onCompleted: () => { _setContactDetails() } })

  const _setContactDetails = () => {
    if (data && data.contact) {
      setEmail(data.contact.email)
      setName(data.contact.name)
    }
  }

  const [updateContact] = useMutation(UPDATE_CONTACT, { onCompleted: () => _redirectToHomeScreen() })

  const _redirectToHomeScreen = () => {
    props.history.push('/')
  }

  const _handleUpdateContact = () => {
    if (!isNameValid(name)) {
      setNameError(true)
      return false
    }

    if (!isEmailValid(email)) {
      setEmailError(true)
      return false
    }

    const contact = {
      id,
      name,
      email
    }

    updateContact({ variables: { contact } })
  }

  const _handleNameInput = (e: any) => {
    nameError && setNameError(false)
    setName(e.target.value)
  }

  const _handleEmailInput = (e: any) => {
    emailError && setEmailError(false)
    setEmail(e.target.value)
  }

  const _renderEmailError = () => {
    return (
      <Alert severity='warning'>
        Invalid email — check it out!
      </Alert>
    )
  }

  const _renderNameError = () => {
    return (
      <Alert severity='warning'>
        Invalid name — field can not be empty!
      </Alert>
    )
  }

  if (loading) return <p>Updating...</p>
  if (error) return <p>Error updating your contact. Please try again.</p>

  return (
    <>
      { data && data.contact ?
        (
          <Container className='contacts-list-container'>
          {nameError && _renderNameError()}
          {emailError && _renderEmailError()}
          <ContactForm
            name={name}
            email={email}
            buttonText='Update contact'
            submitForm={_handleUpdateContact}
            changeName={_handleNameInput}
            changeEmail={_handleEmailInput}
          />
          </Container>
        ) : (
        <Alert severity='error'>Contact not found!</Alert>
      )}
    </>
  )
}

export default UpdateContact