import React, { FC } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { Link } from 'react-router-dom'
import { Container, Grid, IconButton, List, ListItem } from '@material-ui/core'
import { Delete } from '@material-ui/icons'
import { Edit } from '@material-ui/icons'
import Contacts from '../../interfaces/contacts'
import { GET_CONTACTS } from '../../graphQL/queries'
import AddContact from '../AddContact'
import DeleteContact from '../DeleteContact'
import './styles.css'

const ContactsList: FC = (props: any) => {
  const { loading, error, data = { contacts: [] } } = useQuery<Contacts>(GET_CONTACTS, { fetchPolicy: "network-only" })

  if (loading) return <p>Loading...</p>
  if (error) return <p>Error fetching your contacts. Are you running your GQL server?</p>

  const _redirectToUpdateScreen = (e: any, contactId: string) => {
    e.preventDefault()
    props.history.push(`contact/update/${contactId}`)
  }

  return (
    <Container className='contacts-list-container'>
      <AddContact/>
      <List className='contacts-list'>
        {
          data.contacts.map(contact =>
            <Link key={contact.id} to={`/contact/view/${contact.id}`} className='contact-link'>
              <ListItem className='contacts-list-item'>
                <Grid container>
                  <Grid item xs={10} className='contact-details-container'>
                    <span className='contact-details'>Name: {contact.name}</span>
                    <span className='contact-details'>Email: {contact.email}</span>
                  </Grid>

                  <Grid item xs={2} className='contact-actions-container'>
                    <IconButton onClick={(e) => _redirectToUpdateScreen(e, contact.id)}>
                      <Edit/>
                    </IconButton>
                    <DeleteContact contactId={contact.id}>
                      <IconButton >
                        <Delete/>
                      </IconButton>
                    </DeleteContact>
                  </Grid>
                </Grid>
              </ListItem>
            </Link>
          )
        }
      </List>
    </Container>
  )
}

export default ContactsList
