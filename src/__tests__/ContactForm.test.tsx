import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ContactForm, { Props } from '../components/ContactForm'

function renderContactForm(props: Partial<Props> = {}) {
  const defaultProps: Props = {
    name: '',
    email: '',
    buttonText: '',
    submitForm() {
      return
    },
    changeName() {
      return
    },
    changeEmail() {
      return
    }
  }
  return render(<ContactForm {...defaultProps} {...props} />)
}

describe('ContactForm', () => {
  test('should render a blank ContactForm form', async () => {
    const { findByTestId } = renderContactForm()
    const contactForm = await findByTestId('contact-form')

    expect(contactForm).toHaveFormValues({
      name: '',
      email: ''
    })
  })

  test('should allow entering a contact name', async () => {
    const changeName = jest.fn()
    const { findByTestId } = renderContactForm({ changeName })
    const nameInput = await findByTestId('name-input')

    fireEvent.change(nameInput, { target: { value: 'J Doe' } })

    expect(changeName).toHaveBeenCalledWith('J Doe')
  })

  test('should allow entering a contact email', async () => {
    const changeEmail = jest.fn();
    const { findByTestId } = renderContactForm({ changeEmail });
    const emailInput = await findByTestId('email-input');

    fireEvent.change(emailInput, { target: { value: 'test@test.test' } });

    expect(changeEmail).toHaveBeenCalledWith('test@test.test');
  });

  test('should submit the form with provided name and email', async () => {
    const submitForm = jest.fn()
    const { findByTestId } = renderContactForm({ submitForm })
    const nameInput = await findByTestId('name-input')
    const emailInput = await findByTestId('email-input')
    const submit = await findByTestId('submit')

    fireEvent.change(nameInput, { target: { value: 'D Joe' } })
    fireEvent.change(emailInput, { target: { value: 'test@test.com' } })
    fireEvent.click(submit)

    expect(submitForm).toHaveBeenCalledWith('D Joe', 'test@test.com')
  })
})
