import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import { BrowserRouter } from 'react-router-dom'
import ApolloClient from 'apollo-boost'
import Routes from './routes'

const client = new ApolloClient({
  uri: 'http://localhost:3001/'
})

const App = () => (
  <ApolloProvider client={client}>
    <BrowserRouter children={Routes} basename='/' />
  </ApolloProvider>
)

export default App