import { gql } from 'apollo-boost'

export const ADD_CONTACT = gql`
  mutation AddContact($contact: InputContact) {
  	addContact(contact: $contact) {
      id
      name
      email
    }
  }
`

export const DELETE_CONTACT = gql`
  mutation DeleteContact($id: ID) {
    deleteContact(id: $id)
  }
`

export const UPDATE_CONTACT = gql`
  mutation UpdateContact($contact: InputContact) {
    updateContact(contact: $contact) {
      id
      name
      email
    }
  }
`
