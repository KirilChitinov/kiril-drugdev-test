import Contact from './contact'

export default interface Contacts {
  contacts: Array<Contact>
}
